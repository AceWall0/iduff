require 'test_helper'

class SubjectLicensesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subject_license = subject_licenses(:one)
  end

  test "should get index" do
    get subject_licenses_url, as: :json
    assert_response :success
  end

  test "should create subject_license" do
    assert_difference('SubjectLicense.count') do
      post subject_licenses_url, params: { subject_license: { subject_id: @subject_license.subject_id, teacher_id: @subject_license.teacher_id } }, as: :json
    end

    assert_response 201
  end

  test "should show subject_license" do
    get subject_license_url(@subject_license), as: :json
    assert_response :success
  end

  test "should update subject_license" do
    patch subject_license_url(@subject_license), params: { subject_license: { subject_id: @subject_license.subject_id, teacher_id: @subject_license.teacher_id } }, as: :json
    assert_response 200
  end

  test "should destroy subject_license" do
    assert_difference('SubjectLicense.count', -1) do
      delete subject_license_url(@subject_license), as: :json
    end

    assert_response 204
  end
end
