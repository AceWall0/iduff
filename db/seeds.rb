# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# Creation of admin ----------------------------
User.create(name: 'Wallace', 
            email: 'acewall@id.uff.br', 
            password: '123456',
            kind: "admin")


# Creation of teachers -------------------------
10.times do
    User.create(name: Faker::Name.name, 
                email: Faker::Internet.email(domain: "id.uff.br"),
                password: Faker::Internet.password,
                kind: "teacher")
end


# Creation of secretaries -----------------------
5.times do
    User.create(name: Faker::Name.name, 
                email: Faker::Internet.email(domain: "id.uff.br"),
                password: Faker::Internet.password,
                kind: "secretary")
end


# Creation of students -----------------------------------------
100.times do
    User.create(name: Faker::Name.name, 
                email: Faker::Internet.email(domain: "id.uff.br"),
                password: Faker::Internet.password,
                kind: "student")
end


# Creation of subjects and licensing them to a teacher ------
20.times do
    subject = Subject.create(name: Faker::Educator.subject, department: Faker::Educator.campus)
    SubjectLicense.create(teacher_id: rand(1..10), subject: subject)
end