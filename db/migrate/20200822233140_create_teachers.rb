class CreateTeachers < ActiveRecord::Migration[5.2]
  def change
    create_table :teachers do |t|
      t.string :registry
      t.references :user, foreign_key: true
      t.index :registry
      
      t.timestamps
    end
  end
end
