class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.integer :kind, default: 0
      t.string :password_digest
      t.index :email

      t.timestamps
    end
  end
end
