class SubjectLicensesController < ApplicationController
  before_action :set_subject_license, only: [:show, :update, :destroy]
  load_and_authorize_resource

  # GET /subject_licenses
  def index
    @subject_licenses = SubjectLicense.all

    render json: @subject_licenses
  end

  # GET /subject_licenses/1
  def show
    render json: @subject_license
  end

  # POST /subject_licenses
  def create
    @subject_license = SubjectLicense.new(subject_license_params)

    if @subject_license.save
      render json: @subject_license, status: :created, location: @subject_license
    else
      render json: @subject_license.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /subject_licenses/1
  def update
    if @subject_license.update(subject_license_params)
      render json: @subject_license
    else
      render json: @subject_license.errors, status: :unprocessable_entity
    end
  end

  # DELETE /subject_licenses/1
  def destroy
    @subject_license.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject_license
      @subject_license = SubjectLicense.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def subject_license_params
      params.require(:subject_license).permit(:teacher_id, :subject_id)
    end
end
