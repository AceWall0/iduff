class Teacher < ApplicationRecord
  has_many :subject_license
  has_many :subjects, through: :subject_license
  belongs_to :user
  validates :registry, :user_id, presence: true, uniqueness: true
end
