class Student < ApplicationRecord
  belongs_to :user
  validates :registry, :user_id, presence: true, uniqueness: true
end
