class User < ApplicationRecord
    has_secure_password
    validate :only_one_admin_allowed
    validates :name, :email, :kind, presence: true
    validates :name, length: {minimum: 2}
    validates :password_digest, length: {minimum: 6}
    validates :email, uniqueness: {case_sensitive: false}
    has_one :student, dependent: :destroy
    has_one :teacher, dependent: :destroy

    # For more information about how this Regex works, checkout this link
    # where you can hover over every element and see what it does:
    # regexr.com/5am9a
    validates :email, format: {
        with: /[\.\-\w]+@id\.uff\.br\z/i, 
        message: "Email needs to be from @id.uff.br domain"
    }

    # Creates a specialization depending of the kind
    after_save :specialize

    enum kind: {
        student: 0,
        teacher: 1,
        admin: 2,
        secretary: 3,
    }

    def only_one_admin_allowed        
        if kind == 'admin'
            errors.add(:kind, "Only one admin is allowed to exist") if User.find_by(kind: 'admin').present?
        end
    end

    def specialize
        case kind
        when "student"
            Student.create(registry: Faker::IDNumber.brazilian_id, user_id: id)
        when "teacher"
            Teacher.create(registry: Faker::IDNumber.brazilian_id, user_id: id)
        else
        end
    end
end
