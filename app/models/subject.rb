class Subject < ApplicationRecord
    validates :name, :department, length: {minimum: 2}
    validates :name, uniqueness: true
    has_many :subject_license
    has_many :teachers, through: :subject_license
end
