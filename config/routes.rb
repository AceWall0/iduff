Rails.application.routes.draw do
  resources :subject_licenses
  resources :subjects
  get '/current_user', to: 'application#user_must_exist'
  post '/login', to: 'session#login'
  post '/register', to: 'user#create'
  resources :users, except: [:create]
end
